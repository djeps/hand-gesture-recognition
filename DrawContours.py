import cv2
import numpy as np
import sys
from math import ceil

# Initialize the camera (you don't really need this for the given example, but...)
camera = cv2.VideoCapture(0)

if not camera.isOpened():
    print("ERROR: Cannot continue. Camera object is not initialized!")
else:
    # Starting coordinates for the original hand image
    wnd_x_pos = 600
    wnd_y_pos = 250

    # Read the background image and get its dimensions
    mask = cv2.imread('background.png')
    if mask is None:
        sys.exit("ERROR: cannot open 'background.png'. Check the file exists in your script's folder.")
    mask_h, mask_w, mask_ch = mask.shape

    # Read the hand image and get its dimensions
    hand = cv2.imread('hand.png')
    if hand is None:
        sys.exit("ERROR: cannot open 'hand.png'. Check the file exists in your script's folder.")
    hand_h, hand_w, hand_ch = hand.shape

    # Starting coordinates for the masked hand image
    wndcopy_x_pos = wnd_x_pos + hand_w + 35
    wndcopy_y_pos = wnd_y_pos

    # Convert both images to the gray color model
    mask_gray = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
    hand_gray = cv2.cvtColor(hand, cv2.COLOR_BGR2GRAY)

    # Subtract the hand from the background i.e. mask out the hand
    hand_diff = cv2.absdiff(mask_gray, hand_gray)
    hand_diff = cv2.bitwise_not(hand_diff)

    # Apply a blur to the masked image to smooth out the edges
    masked_hand = cv2.GaussianBlur(hand_diff, ksize=(15, 15), sigmaX=0, borderType=cv2.BORDER_DEFAULT)

    # Apply a threshold to filter out the noise - "final" binary representation
    ret, th_masked_hand = cv2.threshold(masked_hand, thresh=25, maxval=255, type=cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # Find all contours in the binary representation of the hand image
    contours, hierarchy = cv2.findContours(th_masked_hand, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)

    max_contour_idx = 0
    max_area = 0
    max_contour = contours[0]

    idx = 0
    for contour in contours:
        area = cv2.contourArea(contour)

        if area > max_area:
            max_area = area
            max_contour = contour
            max_contour_idx = idx

        idx = idx + 1

    # Draw the biggest contour only
    cv2.drawContours(hand, contours=[max_contour], contourIdx=0, color=(0, 255, 0), thickness=2)
    # Compute, then draw the convex hull
    chull = cv2.convexHull(max_contour, returnPoints=False)
    chull_vertices = max_contour[chull]
    chull_vertices_sorted = [] # At the end of the for loop, it won't be really sorted
                               # I'm using the same data container
    for vertice in chull_vertices:
        pt_x = vertice[0][0][0]
        pt_y = vertice[0][0][1]
        pt_vertice = (pt_x, pt_y)
        chull_vertices_sorted.append(pt_vertice)
        print("X = %3d Y = %3d" % (pt_x, pt_y))
        cv2.circle(hand, center=(pt_x, pt_y), radius=10, color=(255,255,255), thickness=2, lineType=8, shift=0)

    print("Convex hull vertices BEFORE the sort:")
    print(chull_vertices_sorted)
    chull_vertices_sorted.sort()
    print("Convex hull vertices AFTER the sort:")
    print(chull_vertices_sorted)

    # Loop through the sorted vertices and look for clusters of vertices
    # i.e. vertices in near proximity to each other
    # Then, remove all vertices in a given cluster EXCEPT
    # the central one
    delta_x = 20
    delta_y = 20
    index = 0
    cnt = 1
    chull_declust_vertices = []
    # We compare each element with the previous one and check if the X and Y coordinates are within limits
    # If the are, we start counting the number of successive elements that belong to a cluster
    # When we encounter an element that's effectively a member of the next cluster
    # we stop and find the middle element of the cluster and add that to our list of 'unique' vertices
    for vertice in chull_vertices_sorted:
        if index > 0:
            if abs(vertice[0] - chull_vertices_sorted[index-1][0]) <= delta_x and abs(vertice[1] - chull_vertices_sorted[index-1][1]) <= delta_y:
                cnt = cnt + 1
            else:
                # Due to the nature we compare i.e. the current element with the previous one
                # in effect - we skip the first element on the first check
                # This is why we go back to the first element as a single member of a cluster
                if index == 1:
                    chull_declust_vertices.append(chull_vertices_sorted[0])
                else:
                    # 'cnt' represents the number of cluster elements
                    # Since we know exactly where we are in the list (from 'index')
                    # and the list is sorted - we take the middle element from the cluster
                    # find it's position in the sorted list and add it to our list of single vertices
                    chull_declust_vertices.append(chull_vertices_sorted[index-ceil(cnt/2)])
                cnt = 1
        index = index + 1

    # Again, due to the nature we compare the elements, when we reach the last element
    # in the loop - we effectively skip it
    # This is where we go back to the last element and correct that
    chull_declust_vertices.append(chull_vertices_sorted[index - ceil(cnt / 2)])

    for vertice in chull_declust_vertices:
        pt_x = vertice[0]
        pt_y = vertice[1]
        pt_vertice = (pt_x, pt_y)
        cv2.circle(hand, center=(pt_x, pt_y), radius=10, color=(0,0,255), thickness=2, lineType=8, shift=0)

    # Find, then draw convexity defects
    cdefects = cv2.convexityDefects(max_contour, chull)
    print("Convex hull deficiencies:")
    print(cdefects)

    # Display the original hand image and its binary representation
    cv2.imshow('HAND_ORIGINAL', hand)
    cv2.moveWindow('HAND_ORIGINAL', wnd_x_pos, wnd_y_pos)
    cv2.imshow('HAND_BINARY', th_masked_hand)
    cv2.moveWindow('HAND_BINARY', wndcopy_x_pos, wndcopy_y_pos)

    # Wait for ESC or 'q' key to terminate the application
    while True:
        key = cv2.waitKey(2)
        
        if (key == 27) or (key == ord('q')):
            break

camera.release()
cv2.destroyAllWindows()
